package com.academiasmoviles.sesion05.model

data class Contacto(
    val nombre: String, val cargo: String, val correo: String, val perfil: String , val url: String){
}