package com.academiasmoviles.sesion05

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.academiasmoviles.sesion05.adapter.ContactoAdapter
import com.academiasmoviles.sesion05.databinding.ActivityMainBinding
import com.academiasmoviles.sesion05.model.Contacto


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    //Definir lista vacia
    private var contactos : MutableList<Contacto> = mutableListOf<Contacto>()
    //Declarar variable ContactoAdapter
    //private  lateinit var adaptador: PokemonAdapter

    private val adaptador by lazy {
        ContactoAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loadData()
        setUpAdapter()
    }

    fun setUpAdapter() {

        binding.rvContacto.adapter= adaptador
        binding.rvContacto.layoutManager = LinearLayoutManager(this)
    }

    fun loadData() {
        //Cargar poke,ones a mi lista
        contactos.add(Contacto("Juan Jose","Android Developer","juanjose@gmail.com","Juan Jose","https://www.pinclipart.com/picdir/big/92-921577_iphone-clipart-smartphone-accessory-celular-sin-fondo-png.png"))
        contactos.add(Contacto("Sergio Caico","Android Developer","sergioc@gmail.com","Sergio Caico","https://www.pinclipart.com/picdir/big/92-921577_iphone-clipart-smartphone-accessory-celular-sin-fondo-png.png"))
        contactos.add(Contacto("Rolando Perez","Android Developer","rperez@gmail.com","Rolando Perez","https://www.pinclipart.com/picdir/big/92-921577_iphone-clipart-smartphone-accessory-celular-sin-fondo-png.png"))
        contactos.add(Contacto("Pedro Lopez","Android Developer","plopez@gmail.com","Pedro Lopez","https://www.pinclipart.com/picdir/big/92-921577_iphone-clipart-smartphone-accessory-celular-sin-fondo-png.png"))
        contactos.add(Contacto("Enrico Caruso","Android Developer","ecaruso@gmail.com","Enrico Caruso","https://www.pinclipart.com/picdir/big/92-921577_iphone-clipart-smartphone-accessory-celular-sin-fondo-png.png"))



        adaptador.actualizarLista(contactos)


    }
}