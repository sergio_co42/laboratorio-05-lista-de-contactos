package com.academiasmoviles.sesion05.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academiasmoviles.sesion05.R
import com.academiasmoviles.sesion05.databinding.ItemContactosBinding
import com.academiasmoviles.sesion05.model.Contacto
import com.squareup.picasso.Picasso

class ContactoAdapter(var contactos: MutableList<Contacto> = mutableListOf()):

    RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>(){

    inner class ContactoAdapterViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        private val binding: ItemContactosBinding= ItemContactosBinding.bind(itemView)
        fun bind(contacto: Contacto){
            binding.tvNombre.text= contacto.nombre
            binding.tvCargo.text=contacto.cargo
            binding.tvCorreo.text=contacto.correo
            binding.tvPerfil.text=contacto.perfil.substring(0,1).toUpperCase()
            Picasso.get().load(contacto.url).into(binding.imgPhone)

        }



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contactos,parent,false)
        return ContactoAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contactos.size
    }

    fun actualizarLista(contactoss: MutableList<Contacto>){
        this.contactos = contactoss
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {

        val contacto = contactos[position]

        holder.bind(contacto)



    }

}
